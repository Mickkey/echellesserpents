﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerpentsEchelles
{
    class CJeuSerpentEchelle
    {
        private string nomJoueur1;
        private string nomJoueur2;
        private int positionJoueur1;
        private int positionJoueur2;
        private int[] tableau;
        private int[] echellesEtSerpents;
        private int[] valeursEchellesEtSerpents;
        private int de;
        private Random rand;

        public void LancerJeu()
        {
            rand = new Random();
            Console.WriteLine("SIMULATION DU JEU SERPENTS ET ECHELLES\n");
            Console.WriteLine("Entrer le nom du joueur 1:");
            nomJoueur1 = Console.ReadLine();
            Console.WriteLine("Entrer le nom du joueur 2:");
            nomJoueur2 = Console.ReadLine();
            positionJoueur1 = 1;
            positionJoueur2 = 1;

            tableau = new int[100];
            echellesEtSerpents = new int[16] { 1, 4, 9, 21, 28, 51, 71, 80, 17,54,62,64,87,93,95,98 };
            valeursEchellesEtSerpents = new int[16] { 38, 14, 31, 42, 84, 67, 91, 100, 7, 34, 19, 60, 24, 73, 75, 79 };

            for(int i = 0; i < 16; i++)
            {
                tableau[echellesEtSerpents[i]] = valeursEchellesEtSerpents[i];
            }
            foreach(int valeur in tableau)
            {
                Console.WriteLine(valeur);
            }


            while (positionJoueur1 < 100 && positionJoueur2 < 100)
            {
                //Tour du joueur 1
                Console.WriteLine("Le joueur {0} lance le dé.", nomJoueur1);
                de = rand.Next(1, 7);

                if(tableau[positionJoueur1 + de] == 0)
                {
                    positionJoueur1 += de;
                    Console.WriteLine("Le joueur {0} a obtenu un {1} et se retrouve à la case {2}", nomJoueur1, de, positionJoueur1);
                }
                else
                {
                    if (positionJoueur1 < tableau[positionJoueur1 + de])
                    {
                        positionJoueur1 = tableau[positionJoueur1 + de];
                        Console.WriteLine("Le joueur {0} a obtenu un {1} et a grimpé une echelle jusqu'à la case {2}.", nomJoueur1, de, positionJoueur1);
                    }
                    else
                    {
                        positionJoueur1 = tableau[positionJoueur1 + de];
                        Console.WriteLine("Le joueur {0} a obtenu un {1} et a glissé sur un serpent jusqu'à la case {2}.", nomJoueur1, de, positionJoueur1);
                    }
                }

                //Tour du joueur 2
                Console.WriteLine("Le joueur {0} lance le dé.", nomJoueur2);
                de = rand.Next(1, 7);
                if (tableau[positionJoueur2 + de] == 0)
                {
                    positionJoueur2 += de;
                    Console.WriteLine("Le joueur {0} a obtenu un {1} et se retrouve à la case {2}", nomJoueur2, de, positionJoueur2);
                }
                else
                {
                    if (positionJoueur2 < tableau[positionJoueur2 + de])
                    {
                        positionJoueur2 = tableau[positionJoueur2 + de];
                        Console.WriteLine("Le joueur {0} a obtenu un {1} et a grimpé une echelle jusqu'à la case {2}.", nomJoueur2, de, positionJoueur2);
                    }
                    else
                    {
                        positionJoueur2 = tableau[positionJoueur2 + de];
                        Console.WriteLine("Le joueur {0} a obtenu un {1} et a glissé sur un serpent jusqu'à la case {2}.", nomJoueur2, de, positionJoueur2);
                    }
                }
            }
            Console.WriteLine("Un joueur a gagné");
        }
    }
}
